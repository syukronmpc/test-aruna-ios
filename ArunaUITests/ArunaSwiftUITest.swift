//
//  ArunaSwiftUITest.swift
//  ArunaUITests
//
//  Created by Syukron on 22/09/22.
//

import XCTest
@testable import Aruna

class the_view_should_presented: XCTestCase {

    func test_should_make_sure_screen_is_displayed() {
        let app = XCUIApplication()
        app.launch()
        
        XCTAssert(app.exists)
    }
}
