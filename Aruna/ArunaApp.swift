//
//  ArunaApp.swift
//  Aruna
//
//  Created by Syukron on 22/09/22.
//

import SwiftUI

@main
struct ArunaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
