//
//  NetworkManager.swift
//  Aruna
//
//  Created by Syukron on 22/09/22.
//

import Foundation

class PostApiService: ObservableObject {
    
    @Published var loading: Bool = false
    @Published var loadingDetail: Bool = false
    @Published var posts: Array<Post> = []
    @Published var postDetail = Post(id: 0, title: "", body: "", userId: 0)
    
    func fetchData() {
        self.loading = true
        if let url = URL(string: "https://jsonplaceholder.typicode.com/posts") {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) {(data, response, error) in
                if (error == nil) {
                    let decoder = JSONDecoder()
                    if let safeData = data {
                        do {
                            let results = try decoder.decode([Post].self, from: safeData)
                            DispatchQueue.main.async {
                                self.posts = results
                                self.loading = false
                            }
                        } catch {
                            self.loading = false
                            print(error)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    func fetchDetail(id: Int) {
        self.loadingDetail = true
        if let url = URL(string: "https://jsonplaceholder.typicode.com/posts/\(id)") {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) {(data, response, error) in
                if (error == nil) {
                    let decoder = JSONDecoder()
                    if let safeData = data {
                        do {
                            let results = try decoder.decode(Post.self, from: safeData)
                            DispatchQueue.main.async {
                                self.loadingDetail = false
                                self.postDetail = results
                            }
                        } catch {
                            self.loadingDetail = false
                            print(error)
                        }
                    }
                }
            }
            task.resume()
        }
    }
}
