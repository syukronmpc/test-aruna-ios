//
//  PostData.swift
//  Aruna
//
//  Created by Syukron on 22/09/22.
//

import Foundation

struct Post: Decodable, Identifiable {
    let id: Int
    let title: String
    let body: String
    let userId: Int
}
