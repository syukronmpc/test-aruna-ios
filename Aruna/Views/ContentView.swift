//
//  ContentView.swift
//  Aruna
//
//  Created by Syukron on 22/09/22.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var postApiService = PostApiService()
    
    var body: some View {
        NavigationView {
            if (postApiService.loading) {
                VStack{
                    ProgressView()
                }.accessibility(identifier: "PostLoading")
            } else {
                List(postApiService.posts) { post in
                    NavigationLink(destination: DetailView(id: post.id)) {
                        Text(post.title)
                    }
                }
                .navigationBarTitle("Posts")
                .accessibility(identifier: "PostList")
            }
        }
        .onAppear {
            self.postApiService.fetchData()
        }.accessibility(identifier: "PostNavigationView")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().previewDevice(PreviewDevice(rawValue: "iPhone 13 Pro"))
    }
}
