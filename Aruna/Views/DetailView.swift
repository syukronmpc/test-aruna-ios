//
//  DetailView.swift
//  Aruna
//
//  Created by Syukron on 22/09/22.
//

import SwiftUI

struct DetailView: View {
    
    let id: Int
    @ObservedObject var postApiService = PostApiService()
    
    var body: some View {
        ScrollView{
            VStack(
                alignment: .center
            ){
                if (postApiService.loadingDetail) {
                    ProgressView()
                } else {
                    VStack(alignment: .leading) {
                        Text(postApiService.postDetail.title)
                            .font(.system(size: 20))
                            .fontWeight(Font.Weight.bold)
                            .padding(.horizontal)
                        
                        Text(postApiService.postDetail.body)
                            .padding(.all)
                            .font(.system(size: 14))
                    }
                }
            }
        }
        .frame(
            maxWidth: .infinity,
            maxHeight: .infinity,
            alignment: Alignment.center
        )
        .onAppear {
            self.postApiService.fetchDetail(id: id)
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(id: 1)
    }
}
